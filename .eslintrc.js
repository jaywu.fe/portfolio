module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
    extends: [
        'plugin:vue/recommended',
        'eslint:recommended',
        'airbnb-base',
    ],
    rules: {
        'no-control-regex': 'error',
        'no-empty-character-class': 'error',
        'no-invalid-regexp': 'error',
        'no-console': 'warn',
        'no-debugger': 'error',
        'no-dupe-args': 'error',
        'no-dupe-keys': 'error',
        'no-duplicate-case': 'error',
        'no-ex-assign': 'error',
        'no-extra-semi': 'error',
        'no-irregular-whitespace': 'warn',
        'no-proto': 'error',
        'no-unexpected-multiline': 'error',
        'no-unreachable': 'error',
        'valid-typeof': 'error',
        'no-new': 'warn',
        camelcase: 'off',
        'func-names': 'off',
        'no-fallthrough': 'error',
        'no-redeclare': 'error',
        'comma-spacing': 'error',
        'eol-last': 0,
        eqeqeq: [
            'error',
            'always',
        ],
        indent: [
            'error', 4, {
                SwitchCase: 2,
            },
        ],
        'keyword-spacing': 'error',
        'vue/max-attributes-per-line': ['error', {
            singleline: 10,
            multiline: {
                max: 10,
                allowFirstLine: false,
            },
        }],
        'max-len': [
            'warn',
            160,
            2,
        ],
        'new-parens': 'error',
        'no-mixed-spaces-and-tabs': 'error',
        'no-multiple-empty-lines': [
            'error',
            {
                max: 10,
            },
        ],
        'no-trailing-spaces': 'error',
        'object-curly-spacing': [
            'error',
            'always',
        ],
        quotes: [
            'error',
            'single',
        ],
        semi: [
            'error',
            'never',
        ],
        'linebreak-style': [
            'error',
            'unix',
        ],
        'space-before-blocks': [
            'error',
            'always',
        ],
        'space-before-function-paren': [
            'error',
            'never',
        ],
        'space-in-parens': [
            'error',
            'never',
        ],
        'space-infix-ops': 'error',
        'space-unary-ops': 'error',
        'arrow-parens': [
            'error',
            'always',
        ],
        'arrow-spacing': [
            'error',
            {
                before: true,
                after: true,
            },
        ],
        'no-confusing-arrow': 'error',
        'prefer-const': 'error',
        'jsx-quotes': [
            'error',
            'prefer-double',
        ],
        'import/no-unresolved': [
            2,
            {
                commonjs: true,
                amd: true,
            },
        ],
        'import/export': 'error',
        'import/extensions': [
            'error',
            'always',
            {
                js: 'never',
                vue: 'never',
            },
        ],
        'import/no-webpack-loader-syntax': 0,
        strict: [
            'error',
            'global',
        ],
        'no-undef': 'error',
        'no-unused-vars': [
            'error',
            {
                args: 'none',
            },
        ],
        'vue/html-indent': [
            'error',
            4,
            {
                baseIndent: 1,
            },
        ],
    },
    settings: {
        'import/resolver': {
            webpack: {
                config: 'node_modules/@vue/cli-service/webpack.config.js',
            },
            node: {
                extensions: [
                    '.js',
                    '.jsx',
                    '.vue',
                ],
            },
        },
    },
}