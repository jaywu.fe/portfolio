import gsap from 'gsap/dist/gsap.min'
import ScrollTrigger from 'gsap/dist/ScrollTrigger.min'

gsap.registerPlugin(ScrollTrigger)

export default {
    watch: {
        $route() {
            this.$nextTick(() => {
                this.seizeAppHeight()
                ScrollTrigger.refresh()
            })
        },
    },
    mounted() {
        this.initWrapperScroll()
    },
    methods: {
        seizeAppHeight() {
            const scroller = this.$el.querySelector('#scroller')
            this.$el.style.height = `${scroller.offsetHeight}px`
        },
        initWrapperScroll() {
            this.seizeAppHeight()
            gsap.to(this.$el.querySelector('#scroller'), {
                scrollTrigger: {
                    trigger: this.$el,
                    start: 'top top',
                    end: 'bottom bottom',
                    scrub: 1,
                    // onToggle: (self) => console.log('toggled, isActive:', self.isActive),
                    // onUpdate: (self) => {
                    //     console.log('progress:', self.progress.toFixed(3), 'direction:', self.direction, 'velocity', self.getVelocity())
                    // },
                },
                ease: 'none',
                y: '-100%',
                marginTop: '100vh',
            })
        },
    },
}