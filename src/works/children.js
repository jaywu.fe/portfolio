import hand from './hand'
import tail from './tail'

const children = [{
    path: 'hand',
    component: hand,
    name: 'hand',
}, {
    path: 'tail',
    component: tail,
    name: 'tail',
},
]
export default children