/* eslint-disable */
import Home from '../page/Home'
import About from '../page/About'
import Work from '../works/index'
import children from '../works/children'

const routes = [{
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: About,
    },
    {
        path: '/works',
        name: 'Works',
        component: Work,
        children,
        // children: [{
        //     // 当 /user/profile 匹配成功，
        //     // UserProfile 会被渲染在 User 的 <router-view> 中
        //     path: '1',
        //     component: one,
        // }],
    },
]
export default routes