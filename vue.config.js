// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path')

module.exports = {
    lintOnSave: true,

    pages: {
        index: {
            // entry for the page
            entry: 'src/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            filename: 'index.html',
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Jay | Front End Display',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
            meta: {
                appleMobileWebAppStatusBarStyle: '#228B22',
                author: 'Jay, jaywu.fe@gmail.com',
                viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no',
                // Will generate: <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                'theme-color': '#4285f4',
                // Will generate: <meta name="theme-color" content="#4285f4">
            },
        },
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'scss',
            patterns: [
                path.resolve(__dirname, './src/styles/abstracts/_abstracts.scss'),
            ],
        },
    },
}